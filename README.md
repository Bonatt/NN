# Employing a Deep Feedforward Neural Network for Hand Written Digit Recognition

After implementing [my own kNN from scratch](https://gitlab.com/Bonatt/kNN/tree/master/ToyDEAP) I decided to create my 
own neural network from scratch. I thought about what problems I could solve with a neural network and several came to mind:
event discrimination in my ToyDEAP model, the flower identification in the Iris dataset, or something I had recently learned about:
hand written digit recognition. I decided to attempt the latter, though the algorithm I would set out to create should be robust.
This turned out to be a much more difficult task than kNN. I will now assume the reader understands how a fundamental
[artificial neural network](https://en.wikipedia.org/wiki/Artificial_neural_network) works.


Even if the underlying theory was straight-forward, the complete implementation was not. Creating an architecture was trivial,
creating initial biases and weights was trivial, feeding forward was understandable, but the backpropagation for biases 
and weight redefining was not. I asked a friend if they knew of any resources on backpropagation and was directed to
Michael Nielsen's ["How the backpropagation algorithm works"](http://neuralnetworksanddeeplearning.com/chap2.html).
In the previous chapter, however, he wrote a fantastic introduction to neural networks, 
and coincidentally(!) also walked through his own implementation of a neural network for 
hand written digit recongition in Python. Exactly what I needed...
So full transparency here, the bones and muscles of my code are pulled from him.
Fear not, though: I put a lot of effort in to not only understanding his work and rewriting it as my own, 
but plan(ned) to take it a few steps further here and there. 
For pedagogical reasons, I'd much prefer to scrape code than to employ some black box library (sklearn, e.g.).
The code itself contains significant documention.

PS. I did not have time to carry through with as much analysis and expansion as desired (or even to the full extent Michael did), 
but the main program as-is runs and does well enough. I will now briefly discuss some aspects.


## MNIST

There are a few well-known sources of hand written digit data but the most-well-known is definitely from NIST. 
A popular, Modified subset is known as the MNIST dataset. It can be found [here](http://yann.lecun.com/exdb/mnist/).
However, the format is not super user-friendly 
so I found a format that was already in CSV [here](https://pjreddie.com/projects/mnist-in-csv/).
I could have converted this myself but because this dataset and CSV are so popular, I figured someone else
could save me some work. And I was right.

The data is split into a training and a test set:

```
wc -l mnist_train.csv mnist_test.csv

    60000 mnist_train.csv
    10000 mnist_test.csv
    70000 total
```

Each line stores an ID at index 0 and the flat 784 pixels of the originally-28x28 pixel image:

```
head -1 mnist_train.csv

5,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,18,18,18,126,136,175,26,166,255,247,127,0,0,0,0,0,0,0,0,0,0,0,0,30,36,94,154,170,253,253,253,253,253,225,172,253,242,195,64,0,0,0,0,0,0,0,0,0,0,0,49,238,253,253,253,253,253,253,253,253,251,93,82,82,56,39,0,0,0,0,0,0,0,0,0,0,0,0,18,219,253,253,253,253,253,198,182,247,241,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,80,156,107,253,253,205,11,0,43,154,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,14,1,154,253,90,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,139,253,190,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,11,190,253,70,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,35,241,225,160,108,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,81,240,253,253,119,25,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,45,186,253,253,150,27,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,16,93,252,253,187,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,249,253,249,64,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,46,130,183,253,253,207,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,39,148,229,253,253,253,250,182,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,24,114,221,253,253,253,253,201,78,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,23,66,213,253,253,253,253,198,81,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,18,171,219,253,253,253,253,195,80,9,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,55,172,226,253,253,253,253,244,133,11,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,136,253,253,253,212,135,132,16,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
```


## Loader

I created a separate script to load any percent of the data into Python. 
This is called [MNIST_CSVLoader.py](https://gitlab.com/Bonatt/NN/blob/master/MNIST_CSVLoader.py).

The data is loaded and split into a test, validation, and training set:

```python
# Structure:
# t1 = train[a][b][c]
# t2 = train[a][b][b][d] = t1[d]
#  a = Which image? Image a of len(train)
#  b = Pixel data or ID data? 0 = Pixel data, 1 = ID data
#  c = Value of single Pixel, or single ID. If b = 0, c = 0-784; if b = 1, c = 0-9
#  d = 0 only, i.e., train[0][0][0] = [0.0]; train[0][0][0][0] = 0.0
```
```
Loading ./Data/MNIST/CSV/mnist_train.csv...
Loaded ./Data/MNIST/CSV/mnist_train.csv

Loading ./Data/MNIST/CSV/mnist_test.csv...
Loaded ./Data/MNIST/CSV/mnist_test.csv

MNIST:
 Training dataset:   60000
 Testing dataset:    10000
Here:
 Training dataset:   50
 Testing dataset:    10
 Validation dataset: 10
```

For the example above I ran the code asking to import about 0.1% of the files.
The validation set has not yet been employed but I plan to utilize it one day.

Besides loading the file as either a square (28x28) or flat (1x784) list, 
I've also defined a diagnostic/fun function to plot a single image:

```python
>>> CSVLoaderPlot1(train, 0)
```
![One](Train0.png)



## Network

The [NN6.py](https://gitlab.com/Bonatt/NN/blob/master/NN6.py) script defines and models the neural network.
Again, I assume the reader understands how a 
[artificial neural network](https://en.wikipedia.org/wiki/Artificial_neural_network) works. 


### Architecture

There are an infinite number of artificial neural network architectures. Some perform better than others for any given task.
For my task, the architecture looked like this:

```
NN Architecture: [784, 30, 10]
```

Each image pixel (784 total) was represented by an input neuron, 
and each prediction value (0-9) was represented by a single output neuron.
The number of hidden layers (1) and number of neurons (30) for each layer was chosen by Michael. 
But I did not notice a significant increase in efficacy when experimenting with the hidden layer(s) architecture.


### Activation Function

There a few popular [activation functions](https://dwaithe.github.io/blog_20170508.html):

![Activation Function](activationFunctions.png)

The most popular, and the one used here, is a logistic or sigmoid function:
```python
# Sigmoid
def Sigmoid(z):
    # z = [w1*x1+b1,...]
    return 1./(1.+np.exp(-z))
# Derivative of Sigmoid(z)
def dSigmoid(z):
    return Sigmoid(z)*(1-Sigmoid(z))
```

### Stochastic Gradient Descent

As with architectures, there are many methods to minimize the cost function.
For large datasets, gradient descent is often the only realistic method.
More specifically, Michael chose to employ the [stochastic](https://en.wikipedia.org/wiki/Stochastic_gradient_descent) variety: 
Instead of optimizing on the whole dataset, or batch, SGD effectly optimizes on one randomly-chosen chunk, 
or minibatch, of the subset at a time.
The idea is that as long as the minibatch size is large enough, a random sampling should be representative of the full set.
SGD almost always computes the gradient much faster.


#### Hyperparameters

Hyperparameters are parameters that aren't _technically_ part of the neural network but play a significant role in training efficacy.

```
Hyperparameters:
 Number of Epochs: 10
 Minibatch size:   10
 Learning rate:    3.0
```

### Update and Backpropagation

The tricky part of any neural network... I started out originally trying to learn how backpropagation was impleneted when I found
Michael's book thing. At the time of writting the code, I thought I understood; but at the time of writing this README I am fuzzy
on the details. For the sake of precision, I will defer my explanation to him.

### Evaluation

Below is the evaluation using a 0.1% subset of 
the datasets: 50 training instances and 10 test instances:

```
 Epoch          Classification Rate
--------------------------------------
 Epoch 1        1/10 = 10.0%
 Epoch 2        1/10 = 10.0%
 Epoch 3        2/10 = 20.0%
 Epoch 4        1/10 = 10.0%
 Epoch 5        2/10 = 20.0%
 Epoch 6        1/10 = 10.0%
 Epoch 7        1/10 = 10.0%
 Epoch 8        1/10 = 10.0%
 Epoch 9        2/10 = 20.0%
 Epoch 10       2/10 = 20.0%
         128055 function calls in 11.812 seconds
```

But now if we utilize the full dataset: 50000 training instances and 10000 test 
instances:

```
 Epoch          Classification Rate
--------------------------------------
 Epoch 1        9112/10000 = 91.12%
 Epoch 2        9162/10000 = 91.62%
 Epoch 3        9260/10000 = 92.6%
 Epoch 4        9333/10000 = 93.33%
 Epoch 5        9381/10000 = 93.81%
 Epoch 6        9353/10000 = 93.53%
 Epoch 7        9405/10000 = 94.05%
 Epoch 8        9423/10000 = 94.23%
 Epoch 9        9434/10000 = 94.34%
 Epoch 10       9411/10000 = 94.11%
         26662303 function calls in 3059.000 seconds
```

94% accuracy is pretty good, but one can do better, even within the code written here. 
The 2013 record (held by another neural network) is 99.79%.


I've also run the script via profile to diagnose inefficiencies. It's pretty slow. 
This code was originally written in Python 2.7 and I also somehow didn't known how efficient arrays are.
I am sure if I recoded everything with Python 3 efficiencies the code would be twice as fast.

```
   Ordered by: cumulative time

   ncalls  tottime  percall  cumtime  percall filename:lineno(function)
        1    0.000    0.000 3059.000 3059.000 profile:0(train,test,valid,nnarchitecture,nn,nn.SGD = main())
        1    0.000    0.000 3058.953 3058.953 NN6.py:257(main)
        1    0.000    0.000 3058.953 3058.953 <string>:1(<module>)
        1    0.000    0.000 3058.953 3058.953 :0(exec)
        1    2.359    2.359 2930.703 2930.703 NN6.py:36(SGD)
    50000   44.391    0.001 2512.672    0.050 NN6.py:69(Update)
   500000  171.125    0.000 2356.469    0.005 NN6.py:85(BP)
  2900000 1384.406    0.000 1384.406    0.000 :0(dot)
  1500000   32.625    0.000  828.250    0.001 fromnumeric.py:529(transpose)
  1700000   56.547    0.000  806.672    0.000 fromnumeric.py:50(_wrapfunc)
   500000   29.953    0.000  718.031    0.001 fromnumeric.py:37(_wrapit)
   500001   11.609    0.000  674.203    0.001 numeric.py:424(asarray)
   500001  662.594    0.001  662.594    0.001 :0(array)
       20    0.000    0.000  405.891   20.295 NN6.py:125(Evaluate)
       20    6.016    0.301  401.531   20.077 NN6.py:130(<listcomp>)
   200000   17.141    0.000  380.219    0.002 NN6.py:141(FF)
  3400000  147.125    0.000  147.125    0.000 NN6.py:156(Sigmoid)
  1000000   51.156    0.000  131.672    0.000 NN6.py:160(dSigmoid)
        1    1.781    1.781  128.250  128.250 NN6.py:180(LoadData)
        2   31.781   15.891  126.469   63.234 MNIST_CSVLoader.py:27(CSVLoader)
    70000   92.656    0.001   92.656    0.001 MNIST_CSVLoader.py:50(<listcomp>)
   500000   71.594    0.000   71.594    0.000 NN6.py:76(<listcomp>)
  2200000   53.625    0.000   53.625    0.000 :0(zeros)
   500000   13.812    0.000   49.844    0.000 NN6.py:91(<listcomp>)
   500000   31.969    0.000   31.969    0.000 NN6.py:149(dCost)
   500000   16.094    0.000   28.625    0.000 NN6.py:90(<listcomp>)
  2140000   27.312    0.000   27.312    0.000 :0(append)
  2200000   24.266    0.000   24.266    0.000 :0(getattr)
  1500000   17.734    0.000   17.734    0.000 :0(transpose)
   200000    4.250    0.000   15.297    0.000 fromnumeric.py:943(argmax)
    50000   12.969    0.000   13.797    0.000 NN6.py:78(<listcomp>)
   500000   10.406    0.000   10.406    0.000 NN6.py:77(<listcomp>)
       10    1.969    0.197    9.734    0.973 random.py:258(shuffle)
    50000    3.078    0.000    8.016    0.000 NN6.py:72(<listcomp>)
   499990    4.828    0.000    7.766    0.000 random.py:220(_randbelow)
    50000    2.672    0.000    4.719    0.000 NN6.py:73(<listcomp>)
       22    1.078    0.049    4.609    0.210 :0(sum)
   200000    3.969    0.000    3.969    0.000 :0(argmax)
   200020    2.047    0.000    3.453    0.000 NN6.py:136(<genexpr>)
    50000    2.766    0.000    3.281    0.000 NN6.py:79(<listcomp>)
   200001    1.922    0.000    1.922    0.000 fromnumeric.py:1613(shape)
   730869    1.656    0.000    1.656    0.000 :0(getrandbits)
   200000    1.406    0.000    1.406    0.000 :0(index)
   200047    1.344    0.000    1.344    0.000 :0(len)
   499990    1.281    0.000    1.281    0.000 :0(bit_length)
    70000    1.016    0.000    1.016    0.000 MNIST_CSVLoader.py:45(<listcomp>)
    15611    0.156    0.000    0.344    0.000 codecs.py:318(decode)
    15611    0.188    0.000    0.188    0.000 :0(utf_8_decode)
        2    0.141    0.070    0.141    0.070 MNIST_CSVLoader.py:58(<listcomp>)
    70002    0.078    0.000    0.078    0.000 MNIST_CSVLoader.py:36(<genexpr>)
        1    0.047    0.047    0.047    0.047 :0(setprofile)
       10    0.047    0.005    0.047    0.005 NN6.py:57(<listcomp>)
        2    0.016    0.008    0.016    0.008 :0(open)
        1    0.000    0.000    0.000    0.000 fromnumeric.py:2456(prod)
        1    0.000    0.000    0.000    0.000 NN6.py:240(GetHyparameters)
       33    0.000    0.000    0.000    0.000 :0(print)
        0    0.000             0.000          profile:0(profiler)
        1    0.000    0.000    0.000    0.000 NN6.py:27(<listcomp>)
        1    0.000    0.000    0.000    0.000 NN6.py:32(<listcomp>)
        4    0.000    0.000    0.000    0.000 :0(randn)
        1    0.000    0.000    0.000    0.000 NN6.py:17(__init__)
        2    0.000    0.000    0.000    0.000 :0(reader)
        2    0.000    0.000    0.000    0.000 codecs.py:259(__init__)
       10    0.000    0.000    0.000    0.000 :0(format)
        2    0.000    0.000    0.000    0.000 codecs.py:308(__init__)
        2    0.000    0.000    0.000    0.000 :0(nl_langinfo)
        1    0.000    0.000    0.000    0.000 NN6.py:215(GetArchitecture)
        1    0.000    0.000    0.000    0.000 :0(reduce)
        1    0.000    0.000    0.000    0.000 _methods.py:34(_prod)
        2    0.000    0.000    0.000    0.000 _bootlocale.py:23(getpreferredencoding)
```
