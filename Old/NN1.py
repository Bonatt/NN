import sys
sys.path.append('/home/joshua/Other/FunCode')
from ROOTStyleEtc import *

#import random
#from operator import itemgetter
#import time
#import csv
#from collections import Counter







### From http://neuralnetworksanddeeplearning.com/chap1.html

# Perceptron: 
#   Output =  { 0  if  w.x + b <= 0
#             { 1  if  w.x + b  > 0,
#  where w.x is dot product of input weight w and binary input x (0 or 1), and b is perceptron bias. 
#  Output is binary.

# Sigmoid Neuron:
#  "Sigmoid neurons are similar to perceptrons, but modified so that small 
#  changes in their weights and bias cause only a small change in their output. 
#  That's the crucial fact which will allow a network of sigmoid neurons to learn."
#   Output = S(w.x + b),
#  where S(z) = 1/(1+exp(-z)) is the sigmoid function, and x is any (nonbinary) input between 0 and 1.
#  Output is any (nonbinary) number between 0 and 1.







#w = [1,2,3,4,4]
#b = [5,6,7,6,5]




# Image dimensions, pixels
width = 28
height = 28


# Number n of input neurons
'''
The input pixels are greyscale, with a value of 0.0 representing white, 
a value of 1.0 representing black, and in between values representing gradually 
darkening shades of grey.
'''
n_input = width*height
Input = [0]*n_input # == x

# Number of hidden layers
'''
We'll experiment with different values for n.
'''
l_hidden = 1
n_hidden = 15
Hidden = [[0]*n_hidden]*l_hidden # Hidden[l][n] = neuron n in layer l


# Number of output layers
'''
"If the first neuron fires, i.e., has an output ~1, then that will indicate that the network thinks 
the digit is a 0. If the second neuron fires then that will indicate that the network thinks 
the digit is a 1. And so on. A little more precisely, we number the output neurons from 0 through 9, 
and figure out which neuron has the highest activation value. If that neuron is, say, neuron number 6, 
then our network will guess that the input digit was a 6. And so on for the other output neurons."
Also interesting: ctr+f "You might wonder why we use 10 output neurons."
'''
n_output = 10 # 0,1, ..., 9
Output = [0]*n_output # == y = y(x)

































### Activation Function
# Sigmoid


### Cost Function
'''
"What we'd like is an algorithm which lets us find weights and biases so that the output from 
the network approximates y(x) for all training inputs x.
To quantify how well we're achieving this goal we define a cost function."
'''
def QuadraticCostFunction(w,b, n,y,a):
    d = 0
    for x in xrange(n_input):
        d +- (y[x]-a)**2
    return 1/(2*n)*d

#1/(2*n) * sum_x(||y(x)-a||**2)


# "Our training algorithm has done a good job if it can find weights and biases so that C(w,b)~0."
# "We want to find a set of weights and biases which make the cost as small as possible. 
# We'll do that using an algorithm known as gradient descent."
'''
# Define Euclidean distance.
# r = sqrt( (x-x0)**2 + ... ) for 1D. Add (y-y0)**2 for 2D, etc.
def EuclideanDistance(instance1, instance2):
    # instance1, instance2 = [f1, f2, ..., 'id']
    distance = 0
    for f in xrange(nFeatures):
        distance += (instance1[f] - instance2[f])**2
    return distance #sqrt(distance)


# Define Manhattan distance
def ManhattanDistance(instance1, instance2):
    distance = 0
    for f in xrange(nFeatures):
        distance += abs(instance1[f] - instance2[f])
    return distance
'''

'''
"How can we apply gradient descent to learn in a neural network? 
The idea is to use gradient descent to find the weights w and biases b which minimize the cost."
'''

















'''
class NeuralNetwork(object):
    def __init__(self, sizes):
        self.num_layers = len(sizes)
        self.sizes = sizes # I'm not sure what this does, so I've commented it out???
        self.biases = [np.random.randn(y, 1) for y in sizes[1:]]
        self.weights = [np.random.randn(y, x) for x, y in zip(sizes[:-1], sizes[1:])]
'''


### NeuralNetwork class that takes some architecture of form [n_L0,n_L1,...] and outputs random b,w.
# Note to self: maybe try to make all/most lists into generators for performance (ram, proc)?
# From https://stackoverflow.com/questions/47789/generator-expressions-vs-list-comprehension
# "Use list comprs when the result needs to be iterated over multiple times, or where speed is paramount.
#  Use generator exprs where the range is large or infinite."
class NN(object):
    def __init__(self, arch):
        # Total number of layers, including input and output.
        self.nL = len(arch)
        # Biases for each neuron, e.g. L0:n=2, L1:n=3, L2:n=1 -> 3+1=4 total biases (no b for input L0).
        # - randn(L) = [array([a,b,c]), array([d])]
        # - randn(L,1) = [array([[a],[b],[c]]), array([[d]])]. We want this column (~auto-transposed) form?
        # - arch[1:] instead of arch because no b,w for L0.
        self.biases = [np.random.randn(L,1) for L in arch[1:]]
        # Weights for each connection, e.g. L0:n=2, L1:n=3, L2:n=1 -> 2*3+3*1=9 total connections.
        # - arch[:-1] is all but last; arch[1:] is all but first.
        self.weights = [np.random.randn(y, x) for x, y in zip(arch[:-1], arch[1:])]

# Activation function
def Sigmoid(z):
    return 1./(1.+np.exp(-z))



# Test:
a  = [2,3,1]
b = NN(a).biases    # b = [ [[a],[b],[c]], [[d]] ]; b[0] is b of L1
w = NN(a).weights   # w = [ [[a,b],[c,b],[e,f]], [[g,h,i]] ]; w[0] is w between L0 and L1


# It's a matrix such that w_jk is the weight for the connection between the kth neuron in L1, and the jth neuron in L2.

