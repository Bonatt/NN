import sys; sys.path.append('/home/joshua/Other/FunCode'); from ROOTStyleEtc import *
from MNIST_CSVLoader4 import *




### From http://neuralnetworksanddeeplearning.com/chap1.html

### NeuralNetwork class that takes some architecture of form [n_L0,n_L1,...] and outputs random b,w.
# Note to self: maybe try to make all/most lists into generators for performance (ram, proc)?
# From https://stackoverflow.com/questions/47789/generator-expressions-vs-list-comprehension
# "Use list comprs when the result needs to be iterated over multiple times, or where speed is paramount.
#  Use generator exprs where the range is large or infinite."
class NN(object):
    def __init__(self, arch):
        # arch = [int,...]
        # Total number of layers, including input and output.
        self.nL = len(arch)
        # Biases for each neuron, e.g. L0:n=2, L1:n=3, L2:n=1 -> 3+1=4 total biases (no b for input L0).
        # - randn(L) = [array([a,b,c]), array([d])]
        # - randn(L,1) = [array([[a],[b],[c]]), array([[d]])]. We want this column (~auto-transposed) form?
        # - arch[1:] instead of arch because no b,w for L0.
        self.biases = [np.random.randn(L,1) for L in arch[1:]]
        # Weights for each connection, e.g. L0:n=2, L1:n=3, L2:n=1 -> 2*3+3*1=9 total connections.
        # - arch[:-1] is all but last; arch[1:] is all but first.
        # - NN.weights[1] == w_jk is w for connection between kth neuron in L1 and jth neuron in L2.
        # - This backwards ordering somehow allows us to use FF below?
        self.weights = [np.random.randn(j,k) for k,j in zip(arch[:-1], arch[1:])]


    # Feedfoward
    #'''
    def FF(self, a):
        # a = [[a],[b],...]
        # a' = S(wa+b)
        for b, w in zip(self.biases, self.weights):
            a = Sigmoid(np.dot(w,a) + b)
        return a
    '''
    def FF(self, a1):
        # a1 = a = [[a],[b],...]
        # a2 = a' = S(wa+b)
        for b, w in zip(self.biases, self.weights):
            a2 = Sigmoid(np.dot(w,a1) + b)
        return a2
    '''

    # Stochastic Gradient Descent
    def SGD(self, train, epochs, minibatchsize, eta, test=None):
        # train = [(x,y),...] = train data
        # epochs = int; number of epochs to train for
        # minibatchsize = int
        # eta = float = learning rate
        # Optional test = [(y,x),...] = test data; 
        #  the network will be evaluated against the test data after each epoch, 
        #  and partial progress printed. Useful for tracking progress but slows things down substantially.
        for epoch in xrange(epochs):
            random.shuffle(train)
            # Break train data into chunks, e.g.:
            #  td = range(100)
            #  mbs = 10
            #  [td[i:i+mbs] for i in xrange(0, len(td), mbs)] = 
            #   [ [0, 1, 2, 3, 4, 5, 6, 7, 8, 9], [10, 11, 12, 13, 14, 15, 16, 17, 18, 19], ... ]
            minibatches = [train[i:i+minibatchsize] for i in xrange(0, len(train), minibatchsize)]
            for minibatch in minibatches:
                # np.shape(minibatch) = 
                #print minibatch
                #print np.shape(minibatch)
                #print np.shape(minibatch[0]), np.shape(minibatch[1])
                # Update b,w
                self.Update(minibatch, eta)
            if test: 
                print 'Epoch {0}: {1} / {2}'.format(epoch+1, self.Evaluate(test), len(test))
            else:
                print 'Epoch {0} complete'.format(epoch+1)


    # Update the b,w of network by applying gradient descent using BackPropagation to a single minibatch.
    def Update(self, minibatch, eta):
        # minibatch = [ [[pixel list of image 0],[id list of image 0]],  [of 1], [of 2], ... ]
        # eta = learning rate
        nabla_b = [np.zeros(np.shape(b)) for b in self.biases]
        nabla_w = [np.zeros(np.shape(w)) for w in self.weights]
        for pixels,id in minibatch:
            #print 'np.shape(x), np.shape(y) in minibatch'
            #print np.shape(pixels), np.shape(id)
            delta_nabla_b, delta_nabla_w = self.BP(pixels,id)
            nabla_b = [nb+dnb for nb,dnb in zip(nabla_b, delta_nabla_b)]
            nabla_w = [nw+dnw for nw,dnw in zip(nabla_w, delta_nabla_w)]
        self.weights = [w-(eta/len(minibatch))*nw for w,nw in zip(self.weights, nabla_w)]
        self.biases = [b-(eta/len(minibatch))*nb for b,nb in zip(self.biases, nabla_b)]



    # Backpropagation...
    def BP(self, pixels, id):
        #print 'np.shape(x), np.shape(y)'
        #print np.shape(pixels), np.shape(id)
        """Return a tuple ``(nabla_b, nabla_w)`` representing the
        gradient for the cost function C_x.  ``nabla_b`` and
        ``nabla_w`` are layer-by-layer lists of numpy arrays, similar
        to ``self.biases`` and ``self.weights``."""
        nabla_b = [np.zeros(b.shape) for b in self.biases]
        nabla_w = [np.zeros(w.shape) for w in self.weights]
        # feedforward
        activation = pixels
        activations = [pixels] # list to store all the activations, layer by layer
        zs = [] # list to store all the z vectors, layer by layer
        for b, w in zip(self.biases, self.weights):
            #print 'np.shape(w), np.shape(activation), np.shape(b)'
            #print np.shape(w), np.shape(activation), np.shape(b)
            z = np.dot(w,activation) + b
            zs.append(z)
            activation = Sigmoid(z)
            activations.append(activation)
        # backward pass
        delta = self.CostDerivative(activations[-1], id) * dSigmoid(zs[-1])
        nabla_b[-1] = delta
        nabla_w[-1] = np.dot(delta, np.transpose(activations[-2]))
        # Note that the variable l in the loop below is used a little
        # differently to the notation in Chapter 2 of the book.  Here,
        # l = 1 means the last layer of neurons, l = 2 is the
        # second-last layer, and so on.  It's a renumbering of the
        # scheme in the book, used here to take advantage of the fact
        # that Python can use negative indices in lists.
        for l in xrange(2, self.nL):
            z = zs[-l]
            delta = np.dot(np.transpose(self.weights[-l+1]), delta) * dSigmoid(z)
            nabla_b[-l] = delta
            nabla_w[-l] = np.dot(delta, np.transpose(activations[-l-1]))
        return (nabla_b, nabla_w)


    def Evaluate(self, test):
        """Return the number of test inputs for which the neural
        network outputs the correct result. Note that the neural
        network's output is assumed to be the index of whichever
        neuron in the final layer has the highest activation."""
        test_results = [(np.argmax(self.FF(pixels)), ids) for (pixels, ids) in test]
        #print test_results[0]
        # "[[0.0],[0.0],[1.0],...]" to "2"
        #ids = ids.index([1.0])
        # int(x==y) = 1 if x=y, else = 0.
        #print pixels, ids[0]
        return sum(int(guesses == ids.index([1.0])) for (guesses, ids) in test_results)

    def CostDerivative(self, output_activations, y):
        """Return the vector of partial derivatives dC_x/da for the output activations."""
        return (output_activations-y)
    


### Activation functions
# Sigmoid
def Sigmoid(z):
    # z = [w1*x1+b1,...]
    return 1./(1.+np.exp(-z))
# Derivative of Sigmoid(z)
def dSigmoid(z):
    return Sigmoid(z)*(1-Sigmoid(z))









# "Incidentally, when I described the MNIST data earlier, I said it was split into 60,000 training images, 
#  and 10,000 test images. That's the official MNIST description. 
#  Actually, we're going to split the data a little differently. 
#  We'll leave the test images as is, but split the 60,000-image MNIST training set into two parts: 
#  a set of 50,000 images, which we'll use to train our neural network, 
#  and a separate 10,000 image validation set."
filedir = './Data/MNIST/CSV/'
shape = 'flat'
percent = 0.1 #1, 10
train = CSVLoader(filedir+'mnist_train.csv', shape, percent)
test = CSVLoader(filedir+'mnist_test.csv', shape, percent)

vsplit = int(10000*percent/100.)
valid = train[-vsplit:]

tsplit = len(train)-vsplit
train = train[:tsplit]

# Structure:
# t1 = train[a][b][c]
# t2 = train[a][b][b][d] = t1[d]
#  a = Which image? Image a of len(train)
#  b = Pixel data or ID data? 0 = Pixel data, 1 = ID data
#  c = Value of single Pixel, or single ID. If b = 0, c = 0-784; if b = 1, c = 0-9
#  d = 0 only, i.e., train[0][0][0] = [0.0]; train[0][0][0][0] = 0.0




### Number of input neurons
# "The input pixels are greyscale, with a value of 0.0 representing white, a value of 1.0 
#  representing black, and in between values representing gradually darkening shades of grey."
# Assuming one element is representative shape of all elements,
n_input = np.prod(np.shape(train[0][0]))

### Number of hidden layers and number of neurons in those layers in form [n1,n2,...]
# We'll experiment with different values for n
n_hidden = [30]

### Number of output layers
# If the first neuron fires, i.e., has an output ~1, then that will indicate that the network thinks 
#  the digit is a 0. If the second neuron fires then that will indicate that the network thinks 
#  the digit is a 1. And so on. A little more precisely, we number the output neurons from 0 through 9, 
#  and figure out which neuron has the highest activation value. If that neuron is, say, neuron number 6, 
#  then our network will guess that the input digit was a 6. And so on for the other output neurons."
# Also interesting: ctr+f "You might wonder why we use 10 output neurons."
n_output = 10 # 0,1,...,9


### Hyperparameters (w, b are regular parameters)
# Number of epochs
epochs = 30
# Minibatch size
minibatchsize = 1 #10
# Learning rate / step size
eta = 3.0





### Load and do data!
print ''
nnarchitecture = [n_input] + n_hidden + [n_output]
nn = NN(nnarchitecture)
nn.SGD(train, epochs, minibatchsize, eta, test)

'''
w = nn.weights
b = nn.biases
'''




'''
for x,y in test[:2]: print x, '...\n', y, '\n\n'
'''







### Looking at Nielsen's code to compare:
'''
cd /home/joshua/Other/FunCode/MachineLearning/NN/Nielsen/neural-networks-and-deep-learning/src
py
import numpy as np
import mnist_loader
training_data, validation_data, test_data = mnist_loader.load_data_wrapper()
import network
net = network.Network([784, 30, 10])
w = net.weights
'''
