#import sys; sys.path.append('/home/joshua/Other/FunCode'); from ROOTStyleEtc import *
import csv
import numpy as np




# The format is: label, pix-11, pix-12, pix-13, ... where pix-ij is the pixel in the ith row and jth column.
# E.g., "head -1 mnist_test.csv" =  
'''
7,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,84,185,159,151,60,36,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,222,254,254,254,254,241,198,198,198,198,198,198,198,198,170,52,0,0,0,0,0,0,0,0,0,0,0,0,67,114,72,114,163,227,254,225,254,254,254,250,229,254,254,140,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,17,66,14,67,67,67,59,21,236,254,106,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,83,253,209,18,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,22,233,255,83,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,129,254,238,44,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,59,249,254,62,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,133,254,187,5,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,9,205,248,58,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,126,254,182,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,75,251,240,57,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,19,221,254,166,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,203,254,219,35,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,38,254,254,77,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,31,224,254,115,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,133,254,254,52,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,61,242,254,254,52,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,121,254,254,219,40,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,121,254,207,18,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
'''
# represents a handwritten "7" digitized to 28x28 pixels and tranformed to 1x784+1 pixels.





# Image dimensions, pixels
width = 28  # j,x
height = 28 # i,y

# TODO: Make necessary lists into arrays for faster processing, a la Nielsen
### Load MNIST CSV in form above. May output 'square' or 'flat' lists. 
# Flat are conventional in NNs, but one can plot square!
# And percent of data to load; to test without waiting forever for code to run; default = 100
def CSVLoader(file, shape, percent=100):
  # filename = str
  # shape = str = 'square' or 'flat'
  print()
  print('Loading '+file+'...')
  IDs = []
  Pixels = []
  with open(file, 'rt') as f: # Py2.7='rb', Py3='rt'
    images = list(csv.reader(f))
    nimages = sum(1 for row in images)
    #print('Loading '+str(int(percent/100.*nimages))+'/'+str(nimages)+' = ~'+str(percent)+'% of '+file+'...')
    for image in images[:int(percent/100.*nimages)]:
      # Cast image list elements from string to float
      image = list(map(float,image))
      # Parse image list into ID list ([0]) and Pixel list ([1:]); 
      # Create zeros list with 10 elements (matching 0-9 NN output); make z[ID] = 1; append first element to IDs
      z = [0.]*10
      z[int(image[0])] = 1.
      z = [[i] for i in z]
      IDs.append(z)
      # Transform Pixel list from 1x784 to 28x28; normalize from 0-255 into 0.-1.; 
      #  append nonfirst elements to Pixels
      if shape == 'square': Pixels.append(np.reshape([[p/255.] for p in image[1:]],(height,width)))
      if shape == 'flat': Pixels.append([[p/255.] for p in image[1:]])
  # Lists/vectors to matrices ([0,0,...] --> [[0],[0],...]); 
  # a = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
  # b = [[0], [1], [2], [3], [4], [5], [6], [7], [8], [9]]
  # a[0][0] = nope. But b[0][0] = 0 (Note: [column,k,y][row,i,x])
  #IDs = [[i] for i in IDs]
  #Pixels = [[i] for i in Pixels]
  print('Loaded '+file)
  return [[p,id] for p,id in zip(Pixels,IDs)] 



### Plot one image from given square imagelist
'''
# From https://stackoverflow.com/questions/33282368/plotting-a-2d-heatmap-with-matplotlib
# and https://matplotlib.org/examples/color/colormaps_reference.html
def CSVLoaderPlot1(imagelist, image):
  # imagelist = Pixels, e.g,
  # image = 0, e.g.
  import matplotlib.pyplot as plt
  plt.imshow(imagelist[image], cmap='binary', interpolation='none')
  plt.show()
'''
import matplotlib.pyplot as plt
def CSVLoaderPlot1(dataset, index):
  # dataset = "train", e.g.
  # index = "0", e.g.
  imageid = str(np.argmax(dataset[index][1]))
  imageflat = np.array(dataset[index][0]).T
  sq = int(np.sqrt(len(dataset[index][0])))
  imagesquare = np.reshape(imageflat, (sq,sq))
  plt.imshow(imagesquare, cmap='binary', interpolation='none')
  plt.title(imageid)
  plt.show()
  
  





### Examples for loading data. Was mostly for testing:
'''
# Examples 1-3 are obsolete in v4 of this code.
def Example1():
    filedir = './Data/MNIST/CSV/'
    filename = 'mnist_test.csv'
    IDs,Pixels = CSVLoader(filedir+filename, shape='square')
    CSVLoaderPlot1(Pixels,0)
    return IDs,Pixels

def Example2():
    filedir = './Data/MNIST/CSV/'
    filename = 'mnist_test.csv'
    IDs,Pixels = CSVLoader(filedir+filename, shape='flat')
    return IDs,Pixels

def Example3():
    filedir = './Data/MNIST/CSV/'
    filename = 'mnist_test.csv'
    IDs,Pixels = CSVLoader(filedir+filename, shape='flat', percent=10)
    return IDs,Pixels
'''
''' 
import time
start = time.clock()
IDs,Pixels = Example2()
stop = time.clock()
print('Runtime:', stop-start, 's') # = 9.52666 s

start = time.clock()
IDs,Pixels = Example3()
stop = time.clock()
print('Runtime:', stop-start, 's') # = 2.934533 s
'''

def Example4():
    filedir = './Data/MNIST/CSV/'
    filename = 'mnist_test.csv'
    test = CSVLoader(filedir+filename, shape='flat', percent=10)
    return test
'''
test = Example4()
'''






### Nielsen's data structure:
'''
len(training_data) = 50000

i = 0,1,...50000
training_data[i] = ( array([ [0.],[0.9837465],...[784 times] ]), array([ [0.],[1.],...[ten times] ]) )

np.shape(training_data) = (50000, 2)
np.shape(training_data[0]) = (2,)

np.shape(training_data[0][0]) = (784, 1)
np.shape(training_data[0][0][0]) = (1,)
np.shape(training_data[0][0][0][0]) = ()

np.shape(training_data[0][1]) = (10, 1)
np.shape(training_data[0][1][0]) = (1,)
np.shape(training_data[0][1][0][0]) = ()



len(test_data) = 10000

i = 0,1,... 10000
test_data[0] = ( array([ [0.],[0.9837465],...[784 times] ]), 7 )

np.shape(test_data) = (10000, 2)
np.shape(test_data[0]) (2,)
np.shape(test_data[0][0]) = (784, 1)
np.shape(test_data[0][0][0]) = (1,)
np.shape(test_data[0][0][0][0]) = ()

np.shape(test_data[0][1]) = ()
'''
